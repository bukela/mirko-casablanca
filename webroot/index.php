<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="We've got the talent. Web and graphic design.">
    <link rel="icon" href="img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/style.css">
    <title>Casablanca</title>
</head>
<body>
<div class="dropdown">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#links" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
        <span class="fa fa-bars hamburger"></span>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="links">
        <a class="dropdown-item" href="#home">Home</a>
        <a class="dropdown-item" href="#portfolio">Portfolio</a>
        <a class="dropdown-item" href="#clients">Clients</a>
        <a class="dropdown-item" href="#about">Team</a>
        <a class="dropdown-item" href="#contact">Contact us</a>
    </div>
</div>
<section class="home" id="home">
    <div class="jumbotron jumbotron-fluid home">
        <div class="container home-text">
            <h1 class="display-4 text-center">WE'VE GOT THE TALENT</h1>
            <hr class="home">
            <p class="lead text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
            <p class="lead text-center">Aenean commodo ligula eget dolor. Aenean massa.</p>
        </div>
    </div>
</section>

<section id="introduction" class="introduction">
    <div class="container">
        <div class="row">
            <div class="main_intro">
                <div class="intro_title text-center">
                    <h2>INTRODUCTION</h2>
                    <hr class="intro">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                </div>

            </div>
        </div>
    </div>
</section>

<section id="creativity">
    <div class="container creativity">
        <div class="row">
            <div class="main_creativity text-center">
                <a href="" class="btn btn-large">Pure Creativity.</a>
            </div>
        </div>
    </div>
</section>

<section id="portfolio" class="portfolio">
    <div class="container">
        <div class="row">
            <div class="container text-center intro_title">
                <h2>OUR PORTFOLIO</h2>
                <hr class="intro">
            </div>
        </div>
    </div>
    </div>
</section>

<!--porftolio slider-->
<section id="portfolio-work">
    <div class="container">
        <div class="row">
            <div align="center" class="container portfolio-list">
                <button class="btn btn-default filter-button" data-filter="all">All</button>
                <button class="btn btn-default filter-button" data-filter="branding">Branding</button>
                <button class="btn btn-default filter-button" data-filter="web-design">Web design</button>
                <button class="btn btn-default filter-button" data-filter="mobile-ui">Mobile UI</button>
                <button class="btn btn-default filter-button" data-filter="illustration">Illustrations</button>
            </div>
            <br/>



            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter branding">
                <img src="img/pf1.jpg" class="img-responsive">
                <div class="overlay"></div>
                <div class="text text-center">Our work<hr class="portfolio"></div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter web-design || branding">
                <img src="img/pf2.jpg" class="img-responsive">
                <div class="overlay"></div>
                <div class="text text-center">Our work<hr class="portfolio"></div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter branding">
                <img src="img/pf6.jpg" class="img-responsive">
                <div class="overlay"></div>
                <div class="text text-center">Our work<hr class="portfolio"></div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter illustration">
                <img src="img/pf5.jpg" class="img-responsive">
                <div class="overlay"></div>
                <div class="text text-center">Our work<hr class="portfolio"></div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter mobile-ui || web-design">
                <img src="img/pf4.jpg" class="img-responsive">
                <div class="overlay"></div>
                <div class="text text-center">Our work<hr class="portfolio"></div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter illustration || mobile-ui">
                <img src="img/pf3.jpg" class="img-responsive">
                <div class="overlay"></div>
                <div class="text text-center">Our work<hr class="portfolio"></div>
            </div>
        </div>
    </div>
</section>
<!--portfolio slider ende-->


<div class="portfolio_btn container">
    <div class="row">
        <a href="" class="btn btn-md">Show me more</a>
    </div>
</div>


<section id="help">
    <div class="container help">
        <div class="row">
            <div class="main_creativity text-center">
                <a href="" class="btn btn-large">We're here to help.</a>
            </div>
        </div>
    </div>
</section>

<section id="clients" class="client">
    <div class="container">
        <div class="row">
            <div class="container client_title intro_title text-center"><h2>OUR CLIENTS</h2></div>
            <hr class="intro">
        </div>
    </div>
    <div class="container-fluid">
        <div class="row text-center client_row">
            <div class="col-md-2">
                <div class="single_client_logo">
                    <a href=""><img src="/img/clogo1.png" alt="" /></a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="single_client_logo">
                    <a href=""><img src="/img/clogo2.png" alt="" /></a>
                </div>
            </div>
            <div class="col-md-2 ">
                <div class="single_client_logo">
                    <a href=""><img src="/img/clogo3.png" alt="" /></a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="single_client_logo">
                    <a href=""><img src="/img/clogo4.png" alt="" /></a>
                </div>
            </div>
            <div class="col-md-2 ">
                <div class="single_client_logo">
                    <a href=""><img src="/img/clogo5.png" alt="" /></a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="single_client_logo">
                    <a href=""><img src="/img/clogo1.png" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about" class="introduction">
    <div class="container">
        <div class="row">
            <div class="main_intro">
                <div class="intro_title text-center">
                    <h2>ABOUT US</h2>
                    <hr class="intro">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row teaming">
            <div class="col-md-3 team text-center"><img src="img/team1.jpg" class="team_person">
                <div class="text">Team member 1</div>
            </div>
            <div class="col-md-3 team text-center"><img src="img/team2.jpg" class="team_person">
                <div class="text">Team member 2</div>
            </div>
            <div class="col-md-3 team text-center"><img src="img/team3.jpg" class="team_person">
                <div class="text">Team member 3</div>
            </div>
            <div class="col-md-3 text-center"><div class="team_txt" ><h1>WE ARE</h1><h1>HIRING</h1><hr class="intro" ><a href="" class="btn btn-team btn-md">Apply</a></div></div>
        </div>
    </div>
</section>


<!--challenge section start-->
    <div class="container">
        <div class="row snow">
                <div class="hi">This is fully responsive & mobile friendly YouTube video background
                    scaled with 16/9 aspect ratio. Actually bunch of videos - js loads randomly 1 of 4 with different
                    start/end times, then it loops
                    through all the vids.
                    <br><br> Click <span>here</span> to <em>un</em>mute & <span>here</span> to see another vid (<em>0</em> of <em>0</em>). Check all of them! They're quite awesome. ;]</div>
            <div class="tv">
                <div class="screen mute" id="tv"></div>
            </div>
        </div>
    </div>
<!--challenge section end-->
<div class="container derek">
    <div class="row">
        <video width="100%" controls>
            <source src="/video/video1.mp4" type="video/mp4">
            Your browser does not support HTML5 video.
        </video>
    </div>
</div>

<section id="video">
    <div class="container video">
        <div class="row">
            <div class="icon">
                <a href="https://www.youtube.com/embed/kDyJN7qQETA" class=""><img src="./img/playicon.png" alt="play-icon"  /></a>

            </div>
        </div>
    </div>
</section>

<section id="choose" class="introduction">
    <div class="container">
        <div class="row">
            <div class="main_intro">
                <div class="intro_title text-center">
                    <h2>WHY CHOOSE US</h2>
                    <hr class="intro">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                </div>

            </div>
        </div>
    </div>
</section>

<div class="container text-center">
    <div class="row">
        <div class="col-md-4">
            <div class="">
                <div class="">
                    <img src="./img/app.png" alt="" />
                </div>
                <br>
                <div class="">
                    <h5>App Design</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        ilabore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="">
                <div class="">
                    <img src="./img/app2.png" alt="" />
                </div>
                <br>
                <div class="">
                    <h5>Responsive Layout</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        ilabore et dolore.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="">
                <div class="">
                    <img src="./img/makbook.png" alt="" />
                </div>
                <br>
                <div class="">
                    <h5>Pixel Perfect Design</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        ilabore et dolore.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="quote">
    <div class="container quote">
        <div class="row">
            <div class="main_creativity text-center">
                <a href="" class="btn btn-large">Get a quote now.</a>
            </div>
        </div>
    </div>
</section>

<section id="touch" class="introduction">
    <div class="container">
        <div class="row">
            <div class="main_intro">
                <div class="intro_title text-center">
                    <h2>GET IN TOUCH</h2>
                    <hr class="intro">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                </div>

            </div>
        </div>
    </div>
</section>
<section id="contact">
    <div class="container-fluid main_contact">
        <div class="row">
            <div class="col-md-4 address text-center">
                <h4>OUR LOCATION</h4>
                <ul class="address-list">
                    <li>174 Mimosa ST. NW</li>
                    <li>Casablanca, MA 20370</li>
                    <li>(212) 123 456 7</li>
                    <li>Samir Timezguida</li>
                </ul>

                <div>
                    <a href=""><i class="fa fa-twitter contact"></i></a>
                    <a href=""><i class="fa fa-facebook contact"></i></a>
                    <a href=""><i class="fa fa-linkedin contact"></i></a>
                    <a href=""><i class="fa fa-vine contact"></i></a>
                    <a href=""><i class="fa fa-behance contact"></i></a>
                </div>

            </div>
            <div class="col-md-8 message">
                <form id="formid">

                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="First name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="3" placeholder="Message"></textarea>
                    </div>

                    <div class="text-right">
                        <input type="submit" value="SEND" class="btn btn-md blue">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer id="footer" class="footer">
    <div class="container">
        <div class="main_footer text-center">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer_logo">
                        <a href=""><img src="img/logo.png" alt="designer-logo" /></a>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="copyright_text">
                        <p>Made with <i class="fa fa-laptop"></i> and <a href="https://v4-alpha.getbootstrap.com">Bootstrap</a>2017. All Rights Reserved</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/challenge.js"></script>
<script src="js/scroll.js"></script>

</body>
</html>